import mimetypes
import os
from django.views.generic import ListView
from .models import *
from django.http import HttpResponse, FileResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.views.generic import TemplateView


def home(request):
    return render(request, 'lesson_3/home.html')


def task_1(request):
    lets_do_it = [{'priority': 100, 'task': 'Составить список дел'}, {'priority': 150, 'task': 'Изучать Django'},
                  {'priority': 1, 'task': 'Подумать о смысле жизни'}]
    return render(request, 'lesson_3/task1.html', {'lets_do_it_list': lets_do_it})


def task_2(request):
    return render(request, 'lesson_3/task2.html')


def task_2_name(request, id):
    posts = lucasfilm.objects.all().filter(pk=id)
    return render(request, 'lesson_3/task2_name.html', {'posts': posts})


def task_3(request):
    if request.method == 'POST':
        name = request.POST['password']
        if name == '223':
            base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
            filename = 'test.txt'
            filepath = base_dir + '/lesson_3/files/lesson_3/document/' + filename
            path = open(filepath, 'r')
            mime_type, _ = mimetypes.guess_type(filepath)
            response = HttpResponse(path, content_type=mime_type)
            response['Content-Disposition'] = "attachment; filename=%s" % filename
            return response
        else:
            return render(request, 'lesson_3/task3.html')
    else:
        return render(request, 'lesson_3/task3.html')


def task_4(request):
    if request.method == 'POST':
        name = request.POST['password']
        if name == '1':
            return JsonResponse({i: i + i for i in range(1, 20)}, safe=False)
        elif name == "2":
            return HttpResponseRedirect("http://127.0.0.1:8000/admin")
        elif name == "3":
            return HttpResponse("You shall not pass!!!")
        else:
            return render(request, 'lesson_3/task3.html')
    else:
        return render(request, 'lesson_3/task3.html')


class HomeViews(ListView):
    def get(self, request, *args, **kwargs):
        return render(request, 'lesson_3/task4.html')

    def post(self, request):
        if request.POST.get('File'):
            return FileResponse(open('lesson_3/files/lesson_3/picture/Unknown.jpeg', 'rb'))
        elif request.POST.get('Json'):
            return JsonResponse({i: i + i for i in range(1, 20)}, safe=False)
        elif request.POST.get('Http'):
            return HttpResponseRedirect("http://127.0.0.1:8000/admin")
        elif request.POST.get('Text'):
            return HttpResponse("Щось подібне на текст")


def task_5(request):
    lets_do_it = [{'priority': 100, 'task': 'Скласти перелік справ'}, {'priority': 150, 'task': 'Вчити Django'},
                  {'priority': 1, 'task': 'Подумати про сенс життя'}]
    return render(request, 'lesson_3/task5.html', {'lets_do_it_list': lets_do_it})


def task_6(request):
    return render(request, 'lesson_3/task6.html')


def task_6_name(request, id):
    posts = lucasfilm.objects.all().filter(pk=id)
    return render(request, 'lesson_3/task6_name.html', {'posts': posts})


def task_7(request):
    vocabulary = [{'name': 'Шаддам IV', 'surname': 'Корріно'}, {'name': 'Пол', 'surname': 'Атрейдес'},
                  {'name': 'Франклін', 'surname': 'Герберт'}]
    return render(request, 'lesson_3/task7.html', {'vocabulary': vocabulary})


class MyTemplateView(TemplateView):
    template_name = "lesson_3/task8.html"

    latest_question_list = [
        {'id': 2,
         'question_text': 'Що первинне: дух чи матерія?'},
        {'id': 3,
         'question_text': 'Чи існує свобода волі?'},
        {'id': 1,
         'question_text': 'У чому сенс життя?'},
    ]

    def get_context_data(self, **kwargs):
        return {'latest_question_list': self.latest_question_list}
