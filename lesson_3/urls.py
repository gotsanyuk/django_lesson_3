from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.home, name='home'),
    path('lesson_3/task_1', views.task_1, name='task_1'),
    path('lesson_3/task_2', views.task_2, name='task_2'),
    path('lesson_3/task_2/name/<int:id>/', views.task_2_name, name='task_2_name'),
    path('lesson_3/task_3', views.task_3, name='task_3'),
    path('lesson_3/task_4', views.HomeViews.as_view(), name='task_4'),
    path('lesson_3/task_5', views.task_5, name='task_5'),
    path('lesson_3/task_6', views.task_6, name='task_6'),
    path('lesson_3/task_6/name/<int:id>/', views.task_6_name, name='task_6_name'),
    path('lesson_3/task_7', views.task_7, name='task_7'),
    path('lesson_3/task_8/', views.MyTemplateView.as_view(), name='task_8'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
