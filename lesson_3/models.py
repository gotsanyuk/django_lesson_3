from django.db import models
from django.urls import reverse


class lucasfilm(models.Model):
    title = models.CharField(max_length=300)
    content = models.TextField(blank=True)
    photo = models.ImageField(upload_to='photos/')

    def __str__(self):
        return self.title
